<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rankingsystem Model
 *
 * @method \App\Model\Entity\Rankingsystem get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rankingsystem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rankingsystem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingsystem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingsystem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem findOrCreate($search, callable $callback = null, $options = [])
 */
class RankingsystemTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rankingsystem');
        $this->setDisplayField('Ranking');
        $this->setPrimaryKey('Ranking');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Ranking')
            ->allowEmptyString('Ranking', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Percentage')
            ->requirePresence('Percentage', 'create')
            ->notEmptyString('Percentage');

        return $validator;
    }
}
