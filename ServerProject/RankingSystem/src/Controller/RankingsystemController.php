<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Rankingsystem Controller
 *
 * @property \App\Model\Table\RankingsystemTable $Rankingsystem
 *
 * @method \App\Model\Entity\Rankingsystem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RankingsystemController extends AppController
{
    // /**
    //  * Index method
    //  *
    //  * @return \Cake\Http\Response|null
    //  */
    // public function index()
    // {
    //     $rankingsystem = $this->paginate($this->Rankingsystem);

    //     $this->set(compact('rankingsystem'));
    // }

    // /**
    //  * View method
    //  *
    //  * @param string|null $id Rankingsystem id.
    //  * @return \Cake\Http\Response|null
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function view($id = null)
    // {
    //     $rankingsystem = $this->Rankingsystem->get($id, [
    //         'contain' => []
    //     ]);

    //     $this->set('rankingsystem', $rankingsystem);
    // }

    // /**
    //  * Add method
    //  *
    //  * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    //  */
    // public function add()
    // {
    //     $rankingsystem = $this->Rankingsystem->newEntity();
    //     if ($this->request->is('post')) {
    //         $rankingsystem = $this->Rankingsystem->patchEntity($rankingsystem, $this->request->getData());
    //         if ($this->Rankingsystem->save($rankingsystem)) {
    //             $this->Flash->success(__('The rankingsystem has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The rankingsystem could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('rankingsystem'));
    // }

    // /**
    //  * Edit method
    //  *
    //  * @param string|null $id Rankingsystem id.
    //  * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function edit($id = null)
    // {
    //     $rankingsystem = $this->Rankingsystem->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $rankingsystem = $this->Rankingsystem->patchEntity($rankingsystem, $this->request->getData());
    //         if ($this->Rankingsystem->save($rankingsystem)) {
    //             $this->Flash->success(__('The rankingsystem has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The rankingsystem could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('rankingsystem'));
    // }

    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id Rankingsystem id.
    //  * @return \Cake\Http\Response|null Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $rankingsystem = $this->Rankingsystem->get($id);
    //     if ($this->Rankingsystem->delete($rankingsystem)) {
    //         $this->Flash->success(__('The rankingsystem has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The rankingsystem could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }

    public function result()
    {
         $query = $this->Rankingsystem->find('all');
         $array = $query->toArray();
         $this->set('array1',$array[0]);
         $this->set('array2',$array[1]);
         $this->set('array3',$array[2]);
         $this->set('array4',$array[3]);
         $this->set('array5',$array[4]);
         //debug($query);
    }
}
