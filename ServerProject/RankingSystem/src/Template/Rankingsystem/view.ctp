<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rankingsystem $rankingsystem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rankingsystem'), ['action' => 'edit', $rankingsystem->Ranking]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rankingsystem'), ['action' => 'delete', $rankingsystem->Ranking], ['confirm' => __('Are you sure you want to delete # {0}?', $rankingsystem->Ranking)]) ?> </li>
        <li><?= $this->Html->link(__('List Rankingsystem'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rankingsystem'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rankingsystem view large-9 medium-8 columns content">
    <h3><?= h($rankingsystem->Ranking) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($rankingsystem->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ranking') ?></th>
            <td><?= $this->Number->format($rankingsystem->Ranking) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Percentage') ?></th>
            <td><?= $this->Number->format($rankingsystem->Percentage) ?></td>
        </tr>
    </table>
</div>
