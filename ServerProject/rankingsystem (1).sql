-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techstadium_kadai`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rankingsystem`
--

CREATE TABLE `rankingsystem` (
  `Ranking` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Percentage` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `rankingsystem`
--

INSERT INTO `rankingsystem` (`Ranking`, `Name`, `Percentage`) VALUES
(1, 'JavaScript', 67),
(2, 'HTML/CSS', 63),
(3, 'SQL', 54),
(4, 'Python', 41),
(5, 'Java', 41),
(6, 'Bash/Shell/PowerShell', 36),
(7, 'C#', 31),
(8, 'PHP', 26),
(9, 'C++', 23),
(10, 'TypeScript', 21);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `rankingsystem`
--
ALTER TABLE `rankingsystem`
  ADD PRIMARY KEY (`Ranking`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `rankingsystem`
--
ALTER TABLE `rankingsystem`
  MODIFY `Ranking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
